### Introduction
The Apple Worldwide Developer Conference (WWDC) is an annual conference held by Apple, intended to present new software and technologies to developers and media. As part of the event, Apple organizes a contest in which students from around the world are asked to submit a Swift Playground. Winners of this contest are offered a free invitation to the week-long WWDC event, in San Francisco.

This repo contains the playground I submitted for WWDC18, which was accepted by Apple.

### Fold The Page
The concept is simple: this playground is a small, side-view platforming game in which the player has to find its way to targets in the world. The original and interesting mechanic of the game lets the user interact with the screen as a foldable sheet of paper. Clicking and draging over the screen will create a fold that will reshape it and allow the player to get to previously unreachable platforms.

This playground can be run by simply opening the .playground file in XCode.

Please contact me if you would like to share thoughts, or if you are curious about some details of the project or the event itself. Have fun!
