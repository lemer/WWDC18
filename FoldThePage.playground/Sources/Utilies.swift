import Foundation


public struct PhysicsCategory {
    public static let nothing: UInt32 = 0
    public static let player: UInt32 = 1
    public static let walls: UInt32 = 2
    public static let goals: UInt32 = 4
}

public struct zLayer {
    public static let player: CGFloat = 100
    public static let loadScreen: CGFloat = 1000
    public static let fold: CGFloat = 30
    public static let UI: CGFloat = 200
}



extension CGPoint {
    public static func +(left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x + right.x,
                       y: left.y + right.y)
    }
    public static func -(left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x - right.x,
                       y: left.y - right.y)
    }
    public static func *(left: CGPoint, right: CGFloat) -> CGPoint {
        return CGPoint(x: left.x * right,
                       y: left.y * right)
    }
    public static prefix func -(point: CGPoint) -> CGPoint {
        return CGPoint(x: -point.x, y: -point.y)
    }
    
    public func distanceTo(_ other: CGPoint) -> CGFloat {
        let x = other.x - self.x
        let y = other.y - self.y
        return (x * x + y * y).squareRoot()
    }
    public func normalize() -> CGPoint {
        let m = magnitude()
        return CGPoint(x: x/m, y: y/m)
    }
    public func magnitude() -> CGFloat {
        return (x * x + y * y).squareRoot()
    }
    public func vector() -> CGVector {
        return CGVector(dx: self.x,
                        dy: self.y)
    }
    public func rotated(by angle: CGFloat) -> CGPoint {
        let new_pt = CGPoint(x: x * cos(angle) - y * sin(angle),
                             y: x * sin(angle) + y * cos(angle))
        return new_pt
    }
}


extension CGVector {
    public func magnitude() -> CGFloat {
        return (self.dx * self.dx + self.dy * self.dy).squareRoot()
    }
    public func dot(_ other: CGVector) -> CGFloat {
        return dx * other.dx + dy * other.dy
    }
    public func angle(with other: CGVector) -> CGFloat {
        return acos(self.dot(other) / (self.magnitude() * other.magnitude()))
    }
}
